#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <termios.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
int sock = 0, readerConn, status = 0, port = 8080;
pthread_t thread;
static struct termios stored_settings;
bool same(char *a, char *b)
{
  for (int i = 0; a[i] != '\0'; i++)
    if (toupper(a[i]) != toupper(b[i]))
      return 0;
  return 1;
}
void removeStr(char *a, char *b)
{
  char *match;
  int len = strlen(b);
  while ((match = strstr(a, b)))
    *match = '\0', strcat(a, match + len);
}
int main(int argc, char const *argv[])
{
  struct sockaddr_in alamatServer;
  char *login = "login", *logout = "logout", *regist = "register";
  char temp, buffer[1100] = {0}, cmd[1100], username[1100], password[1100];
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("Failed to create socket\n");
    return -1;
  }
  memset(&alamatServer, '0', sizeof(alamatServer));
  alamatServer.sin_family = AF_INET, alamatServer.sin_port = htons(port);
  if (inet_pton(AF_INET, "127.0.0.1", &alamatServer.sin_addr) <= 0)
  {
    printf("Invalid address\n");
    return -1;
  }
  if (connect(sock, (struct sockaddr *)&alamatServer, sizeof(alamatServer)) < 0)
  {
    printf("Connection failed\n");
    return -1;
  }
  readerConn = read(sock, buffer, 1100);
  printf("%s\n", buffer);
  while (!same("Connected", buffer))
  {
    printf("Enter anything to check whether you can connect to the server or not.\n");
    scanf("%s", cmd);
    send(sock, cmd, 1100, 0);
    readerConn = read(sock, buffer, 1100);
    printf("%s\n", buffer);
  }
  if (!same("Connected", buffer))
    return 0;
  memset(buffer, 0, sizeof(buffer));
  while (1)
  {
    printf("1. Login\n2. Register\n3. Exit\n");
    scanf("%s", cmd);
    if (same(cmd, login))
    {
      send(sock, login, strlen(login), 0);
      printf("Username: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      memset(buffer, 0, sizeof(buffer));
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "LoginSuccess"))
        printf("Login process is success! Welcome aboard! ^o^\n");
      else
        printf("Login process failed, please try again.\n");
      if (same(buffer, "LoginSuccess"))
      {
        while (1)
        {
          printf("1. Logout\n2. Add\n3. Download [Problem Name]\n4. Submit [Problem Name] [Path Output]\n5. See\n");
          scanf("%s", cmd);
          if (same(cmd, logout))
          {
            send(sock, logout, 1100, 0);
            break;
          }
          else if (same(cmd, "add"))
          {
            send(sock, cmd, 1100, 0);
            char data[1100], problemName[1100];
            printf("Judul Problem: ");
            scanf("\n%[^\n]%*c", problemName);
            send(sock, problemName, 1100, 0);
            printf("Filepath description.txt: ");
            scanf("\n%[^\n]%*c", data);
            char outputFolder[1100], inputFolder[1100], descFolder[1100], outputPath[1100];
            strcpy(descFolder, data);
            removeStr(descFolder, "/description.txt");
            mkdir(descFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath input.txt: ");
            scanf("\n%[^\n]%*c", data);
            strcpy(inputFolder, data);
            removeStr(inputFolder, "/input.txt");
            mkdir(inputFolder, 0777);
            send(sock, data, 1100, 0);
            printf("Filepath output.txt: ");
            scanf("\n%[^\n]%*c", data);
            send(sock, data, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numGacha = atoi(buffer);
            memset(buffer, 0, sizeof(buffer));
            strcpy(outputPath, data);
            FILE *fileManager;
            fileManager = fopen(outputPath, "a+");
            for (int i = 0; i < numGacha; i++)
              readerConn = read(sock, buffer, 1100),
              fprintf(fileManager, "%s", buffer),
              memset(buffer, 0, sizeof(buffer));
            fclose(fileManager);
            printf("Successfully adding problem %s to the server!\n", problemName);
            continue;
          }
          else if (same(cmd, "download"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            scanf("%s", cmd);
            send(sock, cmd, 1100, 0);
            readerConn = read(sock, buffer, 1100);
            int numLine = atoi(buffer);
            char inputTarget[1100];
            readerConn = read(sock, inputTarget, 1100);
            for (int i = 0; i < numLine; i++)
            {
              FILE *fpClient = fopen(inputTarget, "r");
              fpClient = fopen(inputTarget, "a+");
              char fileContent[1100];
              readerConn = read(sock, fileContent, 1100);
              fprintf(fpClient, "%s\n", fileContent), fclose(fpClient);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            char fileReady[] = "File is ready to be downloaded.\n", descTarget[1100];
            if (same(buffer, fileReady))
            {
              memset(buffer, 0, sizeof(buffer));
              readerConn = read(sock, buffer, 1100);
              strcpy(descTarget, buffer);
              printf("Downloading description & input from problem %s..\n", cmd);
              int des_fd = open(descTarget, O_WRONLY | O_CREAT | O_EXCL, 0700);
              if (!des_fd)
                perror("can't open file"), exit(EXIT_FAILURE);
              int file_read_len;
              char buff[1100];
              while (1)
              {
                memset(buff, 0, 1100);
                file_read_len = read(sock, buff, 1100);
                write(des_fd, buff, file_read_len);
                break;
              }
              printf("Successfully downloading the files for problem %s to the desired client directory!\n", cmd);
            }
            memset(buffer, 0, sizeof(buffer));
            continue;
          }
          else if (same(cmd, "submit"))
          {
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            char namaProblem[1100], filePath[1100];
            scanf("%s %s", namaProblem, filePath);
            send(sock, namaProblem, 1100, 0);
            FILE *fpC = fopen(filePath, "r");
            if (!fpC)
            {
              printf("Output file is not exist!\n");
              break;
            }
            char *line = NULL;
            ssize_t len = 0;
            ssize_t fileReader;
            bool checkSubmission = true;
            while ((fileReader = getline(&line, &len, fpC) != -1))
            {
              char outputSubmission[105];
              int i = 0, j = 0;
              while (line[i] != ':')
                i++;
              line[i++] = '\0';
              while (line[i] != '\n')
                outputSubmission[j++] = line[i++];
              outputSubmission[j] = '\0';
              send(sock, outputSubmission, 105, 0);
            }
            memset(buffer, 0, sizeof(buffer));
            readerConn = read(sock, buffer, 1100);
            printf("Your output.txt file for problem %s is submitted!\nResult: %s\n", namaProblem, buffer);
          }
          else if (same(cmd, "see"))
          {
            printf("Problems Available in Online Judge:\n");
            send(sock, cmd, 1100, 0);
            memset(buffer, 0, sizeof(buffer));
            while (readerConn = read(sock, buffer, 1100))
            {
              if (same(buffer, "e"))
                break;
              printf("%s", buffer);
            }
            continue;
          }
          else
          {
            printf("Your command is not available.\n");
            continue;
          }
        }
        memset(buffer, 0, sizeof(buffer));
      }
      memset(buffer, 0, sizeof(buffer));
    }
    else if (same(cmd, regist))
    {
      send(sock, regist, strlen(regist), 0);
      memset(buffer, 0, sizeof(buffer));
      printf("Register\nUsername: ");
      scanf("%c", &temp);
      scanf("%[^\n]", username);
      send(sock, username, 1100, 0);
      printf("Password: ");
      scanf("%c", &temp);
      scanf("%[^\n]", password);
      send(sock, password, 1100, 0);
      readerConn = read(sock, buffer, 1100);
      if (same(buffer, "RegError"))
        printf("Register gagal, username sudah terambil atau password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil.\n");
      if (same(buffer, "Register berhasil"))
        printf("Register berhasil\n");
      memset(buffer, 0, sizeof(buffer));
    }
    else if (same(cmd, "exit"))
    {
      send(sock, "exit", strlen("exit"), 0);
      printf("Closing the client program!\n");
      return 0;
    }
    else
    {
      printf("Your command is not available\n");
      continue;
    }
  }
  return 0;
}

